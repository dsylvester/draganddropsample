window.onload = function () {
    load();
    
};
var id = '';

function load() {
    var bubba = document.getElementById('Bubba');
    var davis = document.getElementById('davis');
    var box1 = document.getElementById('box1');

    bubba.draggable = true;
    bubba.ondragend = onDragEnd;
    bubba.ondragstart = dragStart;
    bubba.ondragover = dragOver;
    bubba.ondrop = dropIt;

    
    davis.draggable = true;
    davis.ondragend = onDragEnd;
    davis.ondragstart = dragStart;
    davis.ondragover = dragOver;
    davis.ondrop = dropIt;
    
    
    box1.ondragend = onDragEnd;
    box1.ondragstart = dragStart;
    box1.ondragover = dragOver;
    box1.ondrop = dropIt;
    
    
    console.log("load event detected!");

}



function dragStart(e) {
    console.log("dragStart");
   
    e.dataTransfer.effectAllowed = "move";
    e.dataTransfer.dropEffect = "move";

    e.dataTransfer.setData("text", e.target.id);

    // e.dataTransfer.setData("text\html", e.target.innerHTML);
    e.target.style.border = '5px solid green';
    // e.dataTransfer.setData("text", 'This is Bubba', 1);
    // console.log(JSON.stringify(e, null, 5));

}

function onDrag(e) {
    //  console.log('ON DRAG: ' + JSON.stringify(e, null, 5));
    // alert(e.target.className);

}

function dropIt(e) {
    e.preventDefault();    
    // var sample = e.dataTransfer.getData('text\html');
    var test = e.dataTransfer.getData('text');
    // console.log('SAMPLE Value: ' + sample);
    console.log('TEST Value: ' + test);
    
    e.target.appendChild(document.getElementById(test));
    
}

function dragOver(event) {
    event.preventDefault();
    console.log(event);
    // var sample = e.dataTransfer.getData('text\html');
    var test = event.dataTransfer.getData('text');
    // console.log('SAMPLE Value: ' + sample);
    console.log('TEST Value: ' + test);
    console.log(event);
    // alert(test);
    // e.target.appendChild(document.getElementById(test));
    
}

function onDragEnd(event) {
    console.log(event);
    // event.target.style.border = 'none';
    event.target.style.width = '200px';
}